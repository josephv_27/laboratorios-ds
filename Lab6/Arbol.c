#include<stdio.h>


//estructura de los nodos que van a pertenecer al arbol
struct nodo{
    int dato;
    struct nodo *der;
    struct nodo *izq;
};

typedef struct nodo nodo;
//funcion para crear el arbol a partir de datos enteros
nodo *crearArbol(int dato){
    nodo *nuevo;
    nuevo = (nodo*)malloc(sizeof(nodo));
    nuevo->dato = dato;
    nuevo->der = NULL;
    nuevo->izq = NULL;

    return nuevo;
}
//funcion para agregar nodos a la derecha de la raiz del arbol
nodo *addDerecha(nodo *nuevo, nodo *raiz){
    raiz->der = nuevo;
    return raiz;
}
//funcion para agregar nodos a la izquierda de la raiz del arbol
nodo *addIzquierda(nodo *nuevo, nodo *raiz){
    raiz->izq = nuevo;
    return raiz;
}
//funcion que imprime el valor de cada nodo
void imprimirNodo(nodo *nodo){
    if(nodo != NULL){
        printf("%d ", nodo->dato);
    }else{
        printf("Nodo vacio");
    }
}
//funcion que orden el arbol, en este caso postOrden
void postOrden(nodo *raiz){
    if(raiz != NULL){
        postOrden(raiz->izq);
        postOrden(raiz->der);
        imprimirNodo(raiz);
    }

}


int main(){
    nodo *raiz;
//arbol
    raiz = crearArbol(1);
    raiz->izq = crearArbol(6);
    raiz->der = crearArbol(2);
    raiz->izq->izq = crearArbol(7);
    raiz->der->izq = crearArbol(3);
    raiz->der->der = crearArbol(8);

    printf("resultado arbol postOrden: \n");
    postOrden(raiz);

    return 0;
}












