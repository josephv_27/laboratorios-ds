#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct estudiante{
	int carnet;
	char nombre[20];
	struct estudiante *sig;
};

struct listaEstudiantes{
	struct estudiante *inicio;
	struct estudiante *final;
};

//Se crea un puntero que recorra toda la lista
struct listaEstudiantes *punteroalista = NULL;

//Se crea un puntero que apunte al nombre y al carnet del nuevo estudiante
struct estudiante* CrearEstudiante(char nombre[20], int carnet){
        struct estudiante *estudianteCreado;
	estudianteCreado =  malloc(sizeof(struct estudiante));
        strcpy (estudianteCreado->nombre, nombre);
        estudianteCreado->carnet =  carnet;
        estudianteCreado->sig = NULL;
        return estudianteCreado;
}

//funcion que muestra la lista desde el menu
void mostrarLista(){
	if(punteroalista == NULL){
		printf("La lista esta vacia\n");
	}else{
		struct estudiante *punteroacabeza = punteroalista->inicio;
		while(punteroacabeza->sig != NULL){
			printf("El nombre del estudiante es %s: \n", punteroacabeza->nombre);
			printf("El carnet del estudiante es %d: \n ", punteroacabeza->carnet);
			punteroacabeza = punteroacabeza->sig;
		}
		printf("El nombre del estudiante es %s: \n", punteroacabeza->nombre);
        	printf("El carnet del estudiante es %d: \n", punteroacabeza->carnet);
	}
}


//funcion que agregar un estudiante al inicio de la lista
void agregarEstudianteInicio(char nombre[20], int carnet){
	struct estudiante *nuevoEstudiante;
        nuevoEstudiante = CrearEstudiante(nombre, carnet);
	if(punteroalista == NULL){
		punteroalista = malloc(sizeof(struct listaEstudiantes));
		punteroalista->inicio = nuevoEstudiante;
		punteroalista->final = nuevoEstudiante;


	}else{
		nuevoEstudiante->sig = punteroalista->inicio;
		punteroalista->inicio = nuevoEstudiante;
	}
}


//funcion que agrega un estudiante al final de la lista
void agregarEstudianteFinal(char nombre[20], int carnet){
	struct estudiante *nuevoEstudiante;
	nuevoEstudiante = CrearEstudiante(nombre, carnet);
	 if(punteroalista == NULL){
        	 punteroalista = malloc(sizeof(struct listaEstudiantes));
                 punteroalista->inicio = nuevoEstudiante;
                 punteroalista->final = nuevoEstudiante;

        }else{
		punteroalista->final->sig = nuevoEstudiante;
                punteroalista->final = nuevoEstudiante;
        }

}

//funcion que busca un estudiante de la lista, mediante un indice
int  obtenerEstudiante(int carnet_digitado, int indice){
	if(punteroalista == NULL){
		printf("La lista esta vacia\n");
		return 0;
	}else{
		struct estudiante *punteroindice;
	        punteroindice = punteroalista->inicio;
		int i = 0;
		while(i != indice){
			punteroindice = punteroindice->sig;
			i++;
		}
		if(carnet_digitado == punteroindice->carnet){
			printf("El carnet  ingresado es correcto\n");
                }else{
               		printf("El carnet ingresado no es correcto\n");
                }

	}
}



//funcion que elimina el estudiante del final de la lista
int eliminarFinal(struct estudiante *anterioraeliminar){
	printf("Se ejecuta la funcion eliminarFinal\n");

	struct estudiante *punteroeliminado = punteroalista->final;
	punteroalista->final = anterioraeliminar;
	anterioraeliminar->sig = NULL;
	free(punteroeliminado);
	printf("Se ha eliminado el ultimo estudiante");
	return 0;

}

//funcion que elimina el estudiante al inicio de la lista
int eliminarPrincipio(){
	printf("Se ejecuta la funcion eliminarPrincipio\n");
	struct estudiante *punteroaprincipio = punteroalista->inicio;
	if(punteroaprincipio->sig == NULL){
		punteroalista->inicio = NULL;
		punteroalista->final = NULL;
		free(punteroaprincipio);
		punteroalista = NULL;
		printf("Se elimino el unico estudiante de la lista");

	}else{
		punteroalista->inicio = punteroaprincipio->sig;
		free(punteroaprincipio);
	}
	return 0;

}

//funcion que elimina estudiantes de la lista, esta llama a eliminarPrincipio y eliminarFinal
int  eliminarEstudiante(int indice){
	if(punteroalista == NULL){
		printf("La lista esta vacia\n");
		return 0;
	}
	if(indice == 0){
		eliminarPrincipio();
		return 0;
	}else{
		struct estudiante *punteroindice;
                punteroindice = punteroalista->inicio;
		int i = 0;
		while(i != indice-1){
                        punteroindice = punteroindice->sig;
                        i++;
		}
		struct estudiante *punteroaeliminar = punteroindice->sig;
		if(punteroaeliminar->sig == NULL){
			eliminarFinal(punteroindice);
			return 0;
		}else{
			punteroindice->sig = punteroaeliminar->sig;
			punteroaeliminar->sig = NULL;
			free(punteroaeliminar);
			printf("Se ha eliminado un estudiante\n");

		}
	}

}



int main(){
	int opcion;
	int carnet_digitado;
	char nombre_digitado[20];
	int indice;

//menu que interactua con el usuario
	while(1){
	printf("1. Insertar estudiante al final de la lista\n");
	printf("2. Insertar estudiante al inicio de la lista\n");
	printf("3. Verificar carnet de estudiante\n");
	printf("4. Eliminar estudiante  de la lista\n");
	printf("5. Salir\n");
	printf("6. Mostrar la lista de Estudiantes\n");
	printf("Cual es el numero de su opcion?\n");
	scanf("%d", &opcion);

//casos que el usuario puede acceder desde el menu
	switch(opcion){

//pregunta al usuario por el nombre y el carnet del estudiante a introduir al final de la lista
	case 1:
		printf("Cual es el nombre del estudiante?\n");
		scanf("%s", nombre_digitado);
		printf("Cual es el carnet del estudiante?\n");
		scanf("%d", &carnet_digitado);
		agregarEstudianteFinal(nombre_digitado, carnet_digitado);
		break;



//pregunta al usuario por el nombre y el carnet del estudiante a introduir al inicio de la lista
	case 2:
		printf("Cual es el nombre del estudiante?\n");
                scanf("%s", nombre_digitado);
                printf("Cual es el carnet del estudiante?\n");
                scanf("%d", &carnet_digitado);
		agregarEstudianteInicio(nombre_digitado, carnet_digitado);
		break;

//pregunta al usuario por el carnet que desea a validar
	case 3:
		printf("Cual es la posicion del carnet a validar?\n");
                scanf("%d", &indice);
                printf("digite el carnet: \n");
                scanf("%d", &carnet_digitado);
		obtenerEstudiante(carnet_digitado, indice);
		break;


//pregunta al usuario por la posicion del estudiante que desea eliminar
	case 4:
		printf("Cual es la posicion del estudiante que desea eliminar?\n");
		scanf("%d", &indice);
		eliminarEstudiante(indice);
		break;

//finaliza la ejecucion del programa
	case 5:
		exit(1);
		break;
//muestra la lista al usuario
	case 6:
		mostrarLista();
		break;
	}


	}

	return 0;
}








