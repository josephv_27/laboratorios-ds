class Nodo:

  def __init__(self, valor = None, siguiente = None):
    self.valor = valor
    self.siguiente = siguiente

class Lista_enlazada:

  def __init__(self):
    self.cabeza = None
    self.longitud = 0

  def agregar_inicio(self, valor):
    temp = Nodo(valor)
    temp.siguiente = self.cabeza
    self.cabeza = temp
    del temp
    self.longitud += 1

  def agregar_final(self, valor):
    temp = self.cabeza
    while temp.siguiente != None:
      temp = temp.siguiente
      self.longitud += 1
    temp.siguiente = Nodo(valor)
    self.longitud += 1

  def buscar_ultimo(self):
    indice = self.cabeza
    while indice.siguiente != None:
      indice = indice.siguiente
    return indice

  def invertir_lista(self):
    if self.cabeza == None:
      return ("Su lista esta vacia")
    else:
      indice = self.cabeza
      anterior = None
      while indice != None:
        siguiente = indice.siguiente
        indice.siguiente = anterior
        anterior = indice
        indice = siguiente
      self.cabeza = anterior


  def buscar_nodo(self, indice_buscado, indice=0):
    buscado = self.cabeza
    while buscado != None:
      if indice == indice_buscado:
        print("Su valor buscado es", buscado.valor)
      buscado = buscado.siguiente
      indice+=1
    return "No existe el indice buscado"

  def eliminar_nodo(self, nodoAEliminar):
    temp = self.cabeza
    anterior = None
    while temp and temp.valor != nodoAEliminar:
      anterior = temp
      temp = temp.siguiente
    if anterior is None:
      self.cabeza = temp.siguiente
    elif temp:
      anterior.siguiente = temp.siguiente
      temp.siguiente = None




  def imprimirLista(self):
    Nodo = self.cabeza
    while Nodo != None:
      print(Nodo.valor, end= "\n")
      Nodo = Nodo.siguiente


miLista = Lista_enlazada()
miLista.agregar_inicio(2)
miLista.agregar_inicio(1)
miLista.agregar_final(3)
miLista.agregar_final(4)
miLista.agregar_final(5)
miLista.agregar_final(6)
miLista.imprimirLista()

miLista.buscar_nodo(2)
miLista.buscar_nodo(4)

miLista.invertir_lista()
miLista.imprimirLista()










