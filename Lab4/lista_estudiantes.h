/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol

    Ejemplos Prácticos: Lista de Estudiantes
    Estudiante: Joseph Valenciano Madrigal
**********************************************************************/
//Definición de Macros
#define LONGITUD_MAXIMA_NOMBRE 50
#define LONGITUD_MAXIMA_CARNET 12

//Definición de Estructuras
typedef struct nodo_estudiante
{
	int carnet;
	char *nombre;

	struct nodo_estudiante *ref_siguiente;
}nodo_estudiante;

typedef struct lista_estudiantes
{
	nodo_estudiante *ref_inicio;
	int cantidad;
}lista_estudiantes;

//Definición de Funciones
/*-----------------------------------------------------------------------
	crear_nodo
	Entradas: No recibe parámetros
	Salidas: Retorna un puntero de tipo nodo_estudiante al nodo creado
	Funcionamiento:
		- Solicita al usuario ingresar Nombre y Carnet.
		- Crea un puntero de tipo nodo_estudiante
		- Le asigna al nodo el nombre y carnet ingresados.
		- El nodo apunta a NULL.
		- retorna el puntero al nuevo nodo.
-----------------------------------------------------------------------*/
nodo_estudiante* crear_nodo();
/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: No recibe parametros.
	Salidas: Retorna la lista.
	Funcionamiento:
        - Inicializa la lista vacia.
        - coloca el puntero al inicio de la lista, pero como es vacia, lo iguala a null.
-----------------------------------------------------------------------*/
void inicializar_lista();
/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: Le entra como parametro un puntero de tipo nodo_estudiante.
	Salidas: Retorna la lista.
	Funcionamiento:
        - primero verifica si la lista esta vacia.
        - Luego hace que el puntero se mueva a la siguiente posicion.
        - indica que esa posicion sera el inicio de la lista.
        - coloca el estudiante en esa posicion.
-----------------------------------------------------------------------*/
void insertar_inicio(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	insertar_final
	Entradas: Le entra como parametro un puntero de tipo nodo_estudiante.
	Salidas: Retorna la lista.
	Funcionamiento:
        - verifica si la lista esta vacia.
        - si la lista esta vacia, llama la funcion insertar_inicio,
        para que coloque al estudiante al inicio de la lista, ya que no tiene elementos.
        - si ya tiene elementos, crea un puntero de tipo nodo_estudiante y lo coloca al inicio de la lista.
        - mientra la siguiente posicion del inicio de la lista, no sea null, se va a seguir recorriendo lista.
        - si la siguiente posicion del primer estudiante es null, coloca el puntero en esa posicion,
        e introduce el nuevo estudiante, es decir el ultimo elemento de la lista.


-----------------------------------------------------------------------*/
void insertar_final(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	borrar_por_indice
	Entradas: Le entra como parametro un entero
	Salidas: retorna la lista.
	Funcionamiento:
        - crea un puntero de tipo nodo_estudiante
        - crea un contador de tipo entero
        - si el inicio de la lista es NULL, es que esta vacia.
        - valida que el indice este en acuerdo con las posiciones de los elementos en la lista.
        - si el indice es igual a 0, el puntero se coloca en la siguiente posicion de la lista,
        e indice que esa sera el nuevo inicio de la lista, y elimina el espacio anterior.
        - mientras que el inicio de la lista no sea null, que el contador siga sumando.
        - y si el contador llega a ser igual al indice, el inicio de la lista pasara a ser el siguiente elemento,
        eliminado asi el anterior.
-----------------------------------------------------------------------*/
void borrar_por_indice(int indice);
 /*-----------------------------------------------------------------------
	buscar_por_indice
	Entradas: Le entra como paramentro un entero
	Salidas: retorna el puntero en la posicion del indice
	Funcionamiento:
        - crea un puntero de tipo nodo_estudiante.
        - si el inicio de lista es null, la lista esta vacia.
        - valida que el indice este en acuerdo con las posiciones de los elementos de la lista.
        - crea un contador de tipo entero.
        - mientra el siguiente elemento de la lista no sea null, y el indice sea igual al contador,
        retorna el elemento en esa posicion.
        - si el indice no es igual contador, se sigue sumando al contador, y se sigue recorriendo la lista.
-----------------------------------------------------------------------*/
nodo_estudiante* buscar_por_indice(int indice);

 /*-----------------------------------------------------------------------
	validar_carnets
	Entradas: Le entra como parametro dos enteros, el carnet guardado y el ingresado por el usuario.
	Salidas: No retorna nada.
	Funcionamiento:
        - una vez encontrado el estudiante en x posicion de la lista,
        verifica si el carnet digitado es igual al carnet almacenado en el sistema,
        - imprime si es correcto o no el carnet del estudiante.
-----------------------------------------------------------------------*/
void validar_carnets(int carnet_almacenado, int carnet_ingresado);
 /*-----------------------------------------------------------------------
	menu
	Entradas: No recibe ningun parametro.
	Salidas: No retorna nada
	Funcionamiento:
        - imprime las opciones que el usuario va a poder solicitar.
        - utiliza un switch para llamar las funciones que se utilizan en cada opcion del menu.
-----------------------------------------------------------------------*/
void menu();
 /*-----------------------------------------------------------------------
	main
	Entradas: No recibe nada como parametro
	Salidas: returna un numero entero.
	Funcionamiento:
        - Llama a la funcion menu, que en este caso va a ser lo primero que se ejecuta del programa.
-----------------------------------------------------------------------*/
int main();
 /*-----------------------------------------------------------------------
	get_user_input
	Entradas: Recibe como parametro una cadena de caracteres.
	Salidas: retorna un buffer
	Funcionamiento:
        - bufferiza los caracteres introducidos por el usuario, para un mejor
        desarrollo del programa y un buen manejo de la memoria.
-----------------------------------------------------------------------*/
char* get_user_input(size_t max_size);
 /*-----------------------------------------------------------------------
	get_user_numerical_input
	Entradas: Recibe como parametro un numero entero
	Salidas: retorna un buffer
	Funcionamiento:
        - bufferiza los enteros introducidos por el usuario, para un mejor
        desarrollo del programa y un buen manejo de la memoria.

-----------------------------------------------------------------------*/
int get_user_numerical_input(size_t max_size);
