#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define verde "\x1b[32m"
#define azul "\033[0;34m"
#define EndColor "\033[0m"
#define rojo "\033[1;31m"


typedef struct persona{
    char nombre[15];
    struct persona* Siguiente;
    struct persona* Amigos;

}persona;

persona* lista_Amigos = NULL;


persona* crearPersona(char nombre[]){
    persona* temp = (persona*) malloc(sizeof (persona));
    temp->Siguiente = NULL;
    temp->Amigos= NULL;
    strcpy(temp->nombre ,nombre);
    return temp;
}



void agregarPersona(char nombre[]){
	persona* nuevoAmigos = crearPersona(nombre);
    if (lista_Amigos == NULL){
        lista_Amigos = nuevoAmigos;
    }else{
		persona* indice = lista_Amigos;
		while(indice->Siguiente != NULL){
			indice = indice->Siguiente;
		}indice->Siguiente = nuevoAmigos;
	}
	printf(verde);
	printf("Se ha agregado a %s correctamente \n\n",nombre);
	printf(EndColor);
}


persona* buscarPersonaPorNombre(char nombre[]){
	persona* indice = lista_Amigos;
	while(indice != NULL){
		if(strcmp(indice->nombre,nombre) == 0){
			return indice;
		}
		indice = indice->Siguiente;
	}return NULL;
}

void agregarAmigoss(char nombre[]){
	char nombreAmigos[15];
	persona* personaConAmigossAgregados = buscarPersonaPorNombre(nombre);

	if (personaConAmigossAgregados == NULL){
		printf(rojo);
		printf("Esa persona no esta agregada \n\n");
		printf(EndColor);
	}else{
		printf("Cual es el nombre del Amigos a agregar?\n");
		scanf("%s",nombreAmigos);

		persona* nuevoAmigos =  crearPersona(nombreAmigos);
		if(personaConAmigossAgregados -> Amigos == NULL){
			personaConAmigossAgregados-> Amigos = nuevoAmigos;
			}else{
				persona* indice = personaConAmigossAgregados -> Amigos;
				while (indice->Siguiente != NULL){
					indice = indice->Siguiente;
				}indice->Siguiente = nuevoAmigos;
			}
		}
}

void imprimirAmigoss(char nombre[]){
	persona* AmigossParaMostrar = buscarPersonaPorNombre(nombre);
	if(AmigossParaMostrar == NULL){
		printf(rojo);
		printf("%s no esta en la lista\n\n", nombre);
		printf(EndColor);
	}else{
		persona* indice = AmigossParaMostrar -> Amigos;
		int contador =1;
		if(indice == NULL){
			printf(rojo);
			printf("%s no tiene Amigoss por el momento.\n\n",nombre);
			printf(EndColor);
			return;
		}
		printf(verde);
		printf("Los Amigoss de %s son: \n",nombre);
		while(indice != NULL){
			printf("%d. %s \n",contador,indice->nombre);
			indice= indice->Siguiente;
			contador++;
		}
		printf("\n");printf(EndColor);
	}
}


void imprimirPersonas(persona* primeraPersona){
	persona* indice = primeraPersona;
	int contador = 1;
	printf(azul);
	if(primeraPersona ==NULL){
		printf(rojo);
		printf("No hay personas usuarias por el momento\n\n");
		printf(EndColor);
		return;
	}
	printf("Las personas usuarias son :\n");
	while(indice != NULL){
		printf("%d. %s \n",contador,indice->nombre);
		indice= indice->Siguiente;
		contador++;
	}printf("\n"); printf(EndColor);
}

int main(){
	int opcion;
	char nombreEntrante[15];
	do{

		printf("1. Para agregar persona \n");
		printf("2. Agregar Amigoss a una persona usuarias\n");
		printf("3. Ver Amigoss de una persona \n");
		printf("4. Para imprimir todas las personas usuarias \n");
		printf("5. Salir\n");
		printf("Seleccione la opcion deseada \n");
		scanf("%d",&opcion);

		switch(opcion){
				case 1:
					printf("Escriba el nombre de la persona a agregar \n");
					scanf("%s",nombreEntrante);
					agregarPersona(nombreEntrante);
					break;
				case 2:
					printf("Escriba el nombre de la persona que desea agregar Amigoss \n");
					scanf("%s",nombreEntrante);
					agregarAmigoss(nombreEntrante);
					break;
				case 3:
					printf("Escriba el nombre de la persona que desea ver sus Amigoss \n");
					scanf("%s",nombreEntrante);
					imprimirAmigoss(nombreEntrante);
					break;
				case 4:
					imprimirPersonas(lista_Amigos);
				default:
					break;
			}
	}while(opcion != 5);
}

